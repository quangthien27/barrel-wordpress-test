<?php

global $post;

?>
<div class="two-up">
  <div class="two-up__container">
    <?php
    $_posts = get_posts( [
      'post_type'      => 'post',
      'post_status'    => 'publish',
      'posts_per_page' => 1,
    ] );

    if ( ! empty( $_posts ) ) {
      $post = array_pop( $_posts );
      setup_postdata( $post );
      the_module( 'post', [
        'show_excerpt' => true,
      ] );
      wp_reset_postdata();
    }
    ?>
  </div>
</div>
