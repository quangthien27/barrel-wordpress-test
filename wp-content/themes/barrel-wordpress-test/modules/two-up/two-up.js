/**
 * Initializes the two-up module.
 * @constructor
 * @param {Object} el - The two-up element.
 */
function TwoUp (el) {
  this.el = el
  console.warn('Initialized `two-up` module.')
}

export default TwoUp
