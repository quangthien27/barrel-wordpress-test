<?php

global $post;

?>
<section class="hero" data-module="hero">
  <div class="hero__wrapper" <?php if ( has_post_thumbnail() ) : ?>style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>');"<?php endif; ?>>
    <div class="hero__container hero__container--featured">
      <div class="hero__col">
        <div class="hero__content">
          <h1>
            <?php if ( ! empty( $index__headline = get_field( 'index__headline' ) ) ) : ?>
              <?php echo $index__headline ?>
            <?php else: ?>
              <?php the_title() ?>
            <?php endif; ?>
          </h1>

          <?php
          // FIXME: Investigate why the_content doesn't work here
          echo wpautop( $post->post_content );
          ?>
        </div>
      </div>

      <?php if ( ! empty( $index__product = get_field( 'index__product' ) ) ) : ?>
        <div class="hero__col">
          <img src="<?php echo $index__product['sizes']['large'] ?>"
               class="hero__img"
               width="<?php echo $index__product['sizes']['large-width'] ?>"
               height="<?php echo $index__product['sizes']['large-height'] ?>"
               alt="<?php echo esc_attr( $index__product['alt'] ?: $index__product['title'] ) ?>"
          >
        </div>
      <?php endif; ?>
    </div>
  </div>

  <?php if ( get_field( 'index__show_latest_post' ) ) : ?>
    <div class="hero__featured-post">
      <div class="hero__container hero__container--large">
        <?php the_module( 'two-up' ); ?>
      </div>
    </div>
  <?php endif; ?>
</section>
